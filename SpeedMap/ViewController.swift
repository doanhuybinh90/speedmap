//
//  ViewController.swift
//  SpeedMap
//
//  Created by Doan Huy Binh on 10/4/16.
//  Copyright © 2016 DiffCat. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate{
    
    @IBOutlet weak var myMap: MKMapView!
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    
    
    var mananger = CLLocationManager()
    var overlay = MKCircle()
    
    var latDelta: CLLocationDegrees = 0.5
    var longDelta: CLLocationDegrees = 0.5
    
    
    @IBAction func zoomOut(_ sender: AnyObject) {
        latDelta = myMap.region.span.latitudeDelta * 2
        longDelta = myMap.region.span.longitudeDelta * 2
        
        let span = MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: longDelta)
        let region = MKCoordinateRegion(center: myMap.region.center, span: span)
        
        
        myMap.setRegion(region, animated: true)

    }
    
    
    @IBAction func zomIn(_ sender: AnyObject) {
        
        latDelta = myMap.region.span.latitudeDelta / 2
        longDelta = myMap.region.span.longitudeDelta / 2
        
        let span = MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: longDelta)
        let region = MKCoordinateRegion(center: myMap.region.center, span: span)
        
        
        myMap.setRegion(region, animated: true)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mananger.delegate = self
        mananger.desiredAccuracy = kCLLocationAccuracyBest
        mananger.requestWhenInUseAuthorization()
        mananger.startUpdatingLocation()
        
        speedLabel.text = "0 km/hr"
        locationLabel.text = ""
        myMap.delegate = self
        
    }
    
    var geoCoder = CLGeocoder()
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let myLocation : CLLocation = locations[0] as CLLocation
        
        let latitude: CLLocationDegrees = myLocation.coordinate.latitude
        let longitude: CLLocationDegrees = myLocation.coordinate.longitude
        
        let span : MKCoordinateSpan = MKCoordinateSpanMake(latDelta, longDelta)
        let location: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        
        
        let region: MKCoordinateRegion = MKCoordinateRegionMake(location, span)
        myMap.setRegion(region, animated: true)
        let speed = Int(max(myLocation.speed, 0) * 3.6)
        
        speedLabel.text = "\(speed) km/hr"
        
        
        geoCoder.reverseGeocodeLocation(myLocation) { (playmarks: [CLPlacemark]?, error:Error?) in
            
            if error != nil
            {
                print("error")
            }
            else
            {
                let playmark = playmarks?.first
                
                let subthoroughFare = playmark?.subThoroughfare != nil ? playmark?.subThoroughfare : ""
                let thoroughFare = playmark?.thoroughfare != nil ? playmark?.thoroughfare : ""
                let country = playmark?.country != nil ? playmark?.country : ""
                
                
                let location = "\(subthoroughFare!) \(thoroughFare!) \(country!)"
                
                self.locationLabel.text = location
                
            }
            
        }
        
        if myMap.overlays.isEmpty == false
        {
            myMap.removeOverlays([overlay])
        }
        overlay = MKCircle(center: location, radius: 30)
        myMap.add(overlay)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        let circleRenderer = MKCircleRenderer(overlay: overlay)
        circleRenderer.strokeColor = UIColor(red: 0, green: 0, blue: 1.0, alpha: 0.6)
        circleRenderer.fillColor = UIColor(red: 0, green: 0, blue: 1.0, alpha: 0.6)
        return circleRenderer
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

